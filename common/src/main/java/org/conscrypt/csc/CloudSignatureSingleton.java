package org.conscrypt.csc;


import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONObject;

import java.util.Base64;


public class CloudSignatureSingleton {

    private static String mCSC_BASE_URI;    // will be setted from Chromium
    private static String mPIN;

    private static final String TAG = "CSCSignManger";
    private static String SIGN_HASH_URI;
    private static String AUTHORIZE_URI;
    private static final String SHA512_ALG_OID = "2.16.840.1.101.3.4.2.3";
    private static final String SHA256_ALG_OID = "2.16.840.1.101.3.4.2.1";
    private static CloudSignatureSingleton single_instance = null;



    public String mAuthorizationToken;
    public String mTokenType;

    private String mCredentialId;
    private String mSAD;
    private byte[] mSignature;
    private String mInitSignOtp;
    private String mSignOtp;
    private String mHash;
    private String mSignAlgo = "1.2.840.113549.1.1.1";  // RSA_ENCR_ALG
    private String mHashAlgo = SHA512_ALG_OID;

    private Logger logger;

    public String getmHashAlgo() {
        return mHashAlgo;
    }

    public void setmHashAlgo(String algorithm) {
        if (algorithm.equals("SHA512withRSA") || algorithm.equals("SHA512withRSA/PSS")) {
            this.mHashAlgo = SHA512_ALG_OID;
        } else if (algorithm.equals("SHA256withRSA") || algorithm.equals("SHA256withRSA/PSS")) {
            this.mSignAlgo = SHA256_ALG_OID;
        }
    }

    public String getmCSC_BASE_URI() {
        return mCSC_BASE_URI;
    }

    public void setmCSC_BASE_URI(String mCSC_BASE_URI) {
        this.mCSC_BASE_URI = mCSC_BASE_URI;
        this.SIGN_HASH_URI = mCSC_BASE_URI + "signatures/signHash";
        this.AUTHORIZE_URI = mCSC_BASE_URI + "credentials/authorize";
    }


    public void setmSignAlgo(String algorithm) {
        this.mSignAlgo = algorithm;
    }

    public String getmSignAlgo() {
        return mSignAlgo;
    }

    public String getmHash() {
        return mHash;
    }

    public void setmHash(String mHash) {
        this.mHash = mHash;
    }

    public String getmCredentialId() {
        return mCredentialId;
    }

    public void setmCredentialId(String mCredentialId) {
        this.mCredentialId = mCredentialId;
    }

    public String getmSAD() {
        return mSAD;
    }

    public void setmSAD(String mSAD) {
        this.mSAD = mSAD;
    }

    public byte[] getmSignature() {
        return mSignature;
    }

    public void setmSignature(byte[] mSignature) {
        this.mSignature = mSignature;
    }

    public String getmPIN() {
        return mPIN;
    }

    public void setmPIN(String mPIN) {
        this.mPIN = mPIN;
    }

    public String getmInitSignOtp() {
        return mInitSignOtp;
    }

    public void setmInitSignOtp(String mInitSignOtp) {
        this.mInitSignOtp = mInitSignOtp;
    }

    public String getmSignOtp() {
        return mSignOtp;
    }

    public void setmSignOtp(String mSignOtp) {
        this.mSignOtp = mSignOtp;
    }

    public String getmAuthorizationToken() {
        return mAuthorizationToken;
    }

    public void setmAuthorizationToken(String mAuthorizationToken) {
        this.mAuthorizationToken = mAuthorizationToken;
    }

    public String getmTokenType() {
        return mTokenType;
    }

    public void setmTokenType(String mTokenType) {
        this.mTokenType = mTokenType;
    }


    // private constructor restricted to this class itself
    private CloudSignatureSingleton() {
        logger = Logger.getLogger(CloudSignatureSingleton.class.getName());
    }

    // static method to create instance of Singleton class
    public static CloudSignatureSingleton getInstance() {
        if (single_instance == null)
            single_instance = new CloudSignatureSingleton();

        return single_instance;
    }


    public byte[] signData() {
        try {
            sendAuthorizeReq(mHash, mSignOtp);
            sendSignHashReq(mHash);
            return mSignature;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public void sendAuthorizeReq(final String hash, final String otp) throws InterruptedException {

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String reqBodyStr = "{\r\n\"credentialID\": \"" + mCredentialId + "\",\r\n\"numSignatures\": 1,\r\n\"hash\":\r\n[\r\n\"" +
                            hash + "\"\r\n],\r\n\"PIN\": \"" + mPIN + "\",\r\n\"OTP\": \"" + otp + "\",\r\n\"clientData\": \"12345678\"\r\n}";
                    String response = CloudSignatureUtils.okhttpSignRequestPost(AUTHORIZE_URI, reqBodyStr, mAuthorizationToken);
                    if (response != null) {
                        JSONObject jsonResponse = new JSONObject(response);
                        String sad = jsonResponse.getString("SAD");
                        mSAD = sad;
                    }
                } catch (Exception e) {
//                    Log.e(TAG, e.toString());
                    logger.log(Level.SEVERE, TAG + " " + e.toString());
                }

            }
        });
        thread.start();
        thread.join();
    }

    public void sendSignHashReq(final String hash) throws InterruptedException {

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String reqBodyStr = "{\r\n\"credentialID\": \"" + mCredentialId + "\",\r\n\"SAD\": \"" + mSAD + "\",\r\n\"hash\":\r\n[\r\n\"" +
                            hash + "\"\r\n],\r\n\"signAlgo\": \"" + mSignAlgo + "\",\r\n\"hashAlgo\": \"" + mHashAlgo + "\"\r\n}";

                    String response = CloudSignatureUtils.okhttpSignRequestPost(SIGN_HASH_URI, reqBodyStr, mAuthorizationToken);
                    if (response != null) {
                        JSONObject jsonResponse = new JSONObject(response);
                        String signature = jsonResponse.getString("signatures");
                        signature = signature.substring(2, signature.length() - 2);
                        signature = signature.replace("\\/", "/");
                        signature = signature.replaceAll("\\\\r\\\\n|\\\\r|\\\\n", "");
                        String newSignature = signature.replaceAll("[\\r\\n]+", "");
                        byte[] byteSignature = Base64.getDecoder().decode(newSignature);
                        mSignature = byteSignature;
                    }
                } catch (Exception e) {
//                    Log.e(TAG, e.toString());
                    logger.log(Level.SEVERE, TAG + " " + e.toString());
                }

            }
        });
        thread.start();
        thread.join();
    }
}