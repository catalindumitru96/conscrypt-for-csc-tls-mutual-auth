Conscrypt for CSC remote signing - A Java Security Provider
========================================

### Introduction

- This is the source code of Conscrypt Android modified for enabling client TLS Authentication using Cloud Signature Consortium remote signing .
- All Conscrypt originally files are from the [Conscrypt project](https://github.com/google/conscrypt/ "Conscrypt source repo").
 - For remote signing process was used [Cloud Signature Consortium, Specificationsfor Remote Signature applications v0.1.7.9](https://cloudsignatureconsortium.org/wp-content/uploads/2020/05/CSC_API_V0_0.1.7.9.pdf "Cloud Signature Consortium").
- This provider was used for enabling remote TLS client authentication integrated into a custom [Chromium project](https://github.com/dumitrucatalin/Chromium-Android-CSC-TLS-MUTUAL-AUTH "Chromium source repo").

### Building Conscrypt
- The stept for building this custom implementation is the same described in the [Conscrypt project](https://github.com/google/conscrypt/ "Conscrypt source repo").